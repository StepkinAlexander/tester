#!/bin/bash
#единственное требование(зависимость) для работы скрипта: должен быть установлен curl
# sudo apt-get install curl

#скрипт для проверки: запущена ли служба (перечислено в scripts, перечислено в таком виде в каком они отображаются в процессах) 
#и если нет перезапускает её (в services перечислены службы как зарегистрированы в системе)
scripts[1]=promovideosd
scripts[2]=zoombyinformerd
scripts[3]=rutubeinformerd
scripts[4]=videoadloggerd
scripts[5]=zoombyparserd
scripts[6]=rutubeparserd

services[1]=promovideos
services[2]=zoombydigger-informer
services[3]=rutubedigger-informer
services[4]=videoadlogger-informer
services[5]=zoombydigger-parser
services[6]=rutubedigger-parser

#если порты отличны от используемых здесь - исправить, индексыы во всех массивах должны соответствовать
ports[1]=8082
ports[2]=8081
ports[3]=8080
ports[4]=8083

#если на хосте работают не все из перечисленных (полный набор) сервисов,
# то убрать индексы из in'а неработающих здесь сервисов
echo '------------------v-----------------------'
echo '~~~~~~~~~searching down services~~~~~~~~~~'
for index in 1 2 3 4 5 6 
do
    out=`ps ax|grep ${scripts[index]}|grep -v grep`
    if [ -z "$out" ]
    then
	echo "Service ${scripts[index]} not started! Trying restart..."
	service ${services[index]} start
    else
	echo "Service ${scripts[index]} works."
    fi
done
echo '~~~~~~~~~~~~~done~~~~~~~~~~~~~~~~~~~~~~~~~'

#также, если какойто сервис неактивен на текущем хосте - исключить из in'а
echo '------------------v-----------------------'
echo '~~~~~~~~~searching zoombies~~~~~~~~~~~~~~~'
for index in 1 2 3 4 
do
    out=`curl localhost:${ports[index]} --silent`
    if [ -z "$out" ]
    then
	echo "Service ${scripts[index]} not started! Trying restart..."
	for process in `ps axh|grep -e ${scripts[index]}|grep -v color|awk '{print $1}'`; do kill -9 $process; done
	service ${services[index]} start
    else
	echo "Service ${scripts[index]} alive."
    fi
done
echo '~~~~~~~~~~~~~done~~~~~~~~~~~~~~~~~~~~~~~~~'
echo '------------------------------------------'
